package com.daina.javaFXproject1.services;

import com.daina.javaFXproject1.tables.Author;
import org.hibernate.Session;

public class Project1Repository {
    public static Author getAuthorById(int id) {
        Session session = SessionManager.getSessionFactory().openSession();
        Author author = session.find(Author.class, id);
        session.close();
        return author;
    }
}

